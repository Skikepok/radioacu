<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Proposal Validation</title>
        <link href="icon.ico" type="image/x-icon" rel="icon" />
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
    </head>
    <body>
      <div class="container">
      	<ul class="nav nav-tabs nav-justified">
    	  <li id="index"><a href="index.php">Proposals</a></li>
    	  <li id="all_songs"><a href="all_songs.php">All Songs</a></li>
    	</ul>
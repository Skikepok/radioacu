<?php
  $mysqli = new mysqli('localhost', 'root', 'jesuisuneradio', 'music');
  $mysqli->set_charset("utf8");
  include "header.php";
?>

		<table class="table table-striped">
			<tr>
				<th>Youtube link</th>
				<th>Status</th>
			</tr>
			<?php
				$props = $mysqli->query("select * from proposals where status = 'Waiting approval...';");
				foreach ($props as $id => $prop)
				{
					$link = htmlentities(base64_decode($prop['youtube_link']));
					echo '<tr>';
					echo '<td>';
					echo "  <a href=\"$link\">$link</a>";
					echo '  <a class="right_float" href="accept.php?id=' . $prop['proposal_id'] . '">';
					echo '    <button type="button" class="btn btn-default">';
					echo '      <span class="glyphicon glyphicon-ok"></span>';
					echo '    </button>';
					echo '  </a>';
					echo '  <a class="right_float" href="refuse.php?id=' . $prop['proposal_id'] . '">';
					echo '    <button type="button" class="btn btn-default">';
					echo '      <span class="glyphicon glyphicon-remove"></span>';
					echo '    </button>';
					echo '  </a>';
					echo '</td>';
					echo '<td>' . $prop['status'] . '</td>';
					echo '</tr>';
				}

				$props = $mysqli->query("select * from proposals where status = 'Waiting Download';");
				foreach ($props as $id => $prop)
				{
					$link = htmlentities(base64_decode($prop['youtube_link']));
					echo '<tr class="warning">';
					echo "<td><a href=\"$link\">$link</a></td>";
					echo '<td>' . $prop['status'] . ': Try number '. $prop['tries'] . '</td>';
					echo '</tr>';
				}

				//$props = $mysqli->query("select * from proposals where status = 'Failed';");
				//foreach ($props as $id => $prop)
				//{
				//	$link = htmlentities(base64_decode($prop['youtube_link']));
				//	echo '<tr class="danger">';
				//	echo "<td><a href=\"$link\">$link</a></td>";
				//	echo '<td>' . $prop['status'] . ', bad link.</td>';
				//	echo '</tr>';
				//}

				//$props = $mysqli->query("select * from proposals where status = 'Refused';");
				//foreach ($props as $id => $prop)
				//{
				//	$link = htmlentities(base64_decode($prop['youtube_link']));
				//	echo '<tr class="danger">';
				//	echo "<td><a href=\"$link\">$link</a></td>";
				//	echo '<td>' . $prop['status'] . '</td>';
				//	echo '</tr>';
				//}

				$props = $mysqli->query("select * from proposals where status = 'Downloaded';");
				foreach ($props as $id => $prop)
				{
					$link = htmlentities(base64_decode($prop['youtube_link']));
					echo '<tr class="success">';
					echo "<td><a href=\"$link\">$link</a></td>";
					echo '<td>' . $prop['status'] . '</td>';
					echo '</tr>';
				}
			?>
		</table>


<?php
  include "footer.php";
?>
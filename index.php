<?php
  $mysqli = new mysqli('localhost', 'root', 'jesuisuneradio', 'music');
  $mysqli->set_charset("utf8");
  $tracks = $mysqli->query("select track_title, play_date from history h left join tracks t on t.track_id = h.track_id order by play_date DESC limit 1;");
  $current_song = $tracks->fetch_row()[0];
  include "header.php";
  include "login.php";
?>

<script type="text/javascript">
    var own = document.getElementById('index');
    own.className = "active";
</script>

<div class="col-md-4">
	<h3>Current Song</h3>
	<ul class="list-group">
		<?php
			echo '<li class="list-group-item">' . $current_song . '</li>';
		?>
	</ul>
</div>
<div class="row">
	<div id="tracks" class="col-md-8">
		<h3>My Votes</h3>

		<table class="table table-striped">
			<tr>
				<th>Track Title</th>
				<th>Remove Vote</th>
			</tr>
			<?php
				$tracks = $mysqli->query("select * from votes v left join tracks t on t.track_id = v.track_id where user_login = '$login' order by track_title;");
				foreach ($tracks as $id => $track)
				{
					echo '<tr>';
					echo '<td>' . $track['track_title'] . '</td>';
					echo '<td>';
					echo '  <a href="unvote.php?track_id=' . $track['track_id'] . '">';
					echo '    <button type="button" class="btn btn-default">';
					echo '        <span class="glyphicon glyphicon-remove-circle"></span>';
					echo '    </button>';
					echo '  </a>';
					echo '</td>';
					echo '</tr>';
				}
			?>
		</table>
	</div>
	<div class="col-md-4 right_float">
		<div id="top">
			<div id="history">
				<h3>5 last played songs</h3>
				<ul class="list-group">
					<?php
						$tracks = $mysqli->query("select track_title, play_date from history h left join tracks t on t.track_id = h.track_id order by play_date DESC limit 5 offset 1;");
						foreach ($tracks as $id => $track)
						{
							echo '<li class="list-group-item">' . $track['track_title'] . '</li>';
						}
					?>
				</ul>
			</div>
			<h3>All votes</h3>
			<div class="scrollable">
				<ul class="list-group">
					<?php
						$best_tracks = $mysqli->query("select t.track_id, track_title, COUNT(user_login) as nb_votes from votes v left join tracks t on t.track_id = v.track_id group by track_title order by nb_votes desc;");
						foreach ($best_tracks as $id => $best_track)
						{
							if (strlen($best_track['track_title']) > 35)
								$name = substr($best_track['track_title'], 0, 32) . "...";
							else
								$name = $best_track['track_title'];

							echo '<li class="list-group-item">';
							echo '  <a href="vote.php?from=index&track_id=' . $best_track['track_id'] . '">';
							echo '    <button type="button" class="btn btn-default">';
							echo '        <span class="glyphicon glyphicon-headphones"></span>';
							echo '    </button>';
							echo '  </a>';
							echo "<span style=\"margin-left: 10px;\">$name</span>";
							echo '<span class="badge">' . $best_track['nb_votes'] . '</span></li>';
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php
  include "footer.php";
?>

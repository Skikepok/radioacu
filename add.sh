#!/bin/sh

IFS='
'

files=`find musics/ -name *.mp3`

nblines=`find musics/ -name *.mp3 | wc -l`

echo 'insert into tracks (track_title, track_path) values'
counter=0
for i in $files; do
    new=`echo $i | sed "s@'@@"`
    if [ $i != $new ]; then
	mv $i $new
    fi
    name=`echo $new | sed 's@.*/\(.*\).mp3@\1@'`
    if [ $counter -eq $(($nblines - 1)) ]; then
	echo "(\"$name\", \"$new\");"
    else
	echo "(\"$name\", \"$new\"),"
    fi
    counter=$(($counter + 1))
done

<?php
  $mysqli = new mysqli('localhost', 'root', 'jesuisuneradio', 'music');
  $mysqli->set_charset("utf8");
  $tracks = $mysqli->query("select track_title, play_date from history h left join tracks t on t.track_id = h.track_id order by play_date DESC limit 1;");
  $current_song = $tracks->fetch_row()[0];
  include "header.php";
?>

<script type="text/javascript">
	var own = document.getElementById('all_songs');
	own.className = "active";
</script>

<div class="row">
	<div class="col-md-4 right_float">
		<div id="history">
			<h3>10 last added songs</h3>
			<ul class="list-group">
				<?php
					$tracks = $mysqli->query("select * from tracks order by track_id DESC limit 10;");
					foreach ($tracks as $id => $track)
					{
						$name = $track['track_title'];
						echo '<li class="list-group-item">';
						echo '  <a href="vote.php?from=index&track_id=' . $track['track_id'] . '">';
						echo '    <button type="button" class="btn btn-default">';
						echo '        <span class="glyphicon glyphicon-headphones"></span>';
						echo '    </button>';
						echo '  </a>';
						echo "<span style=\"margin-left: 10px;\">$name</span></li>";
					}
				?>
			</ul>
		</div>
	</div>
	<div id="tracks" class="col-md-8">
		<table class="table table-striped">
			<tr>
				<th>Track Title</th>
				<th>Vote</th>
			</tr>
			<?php
				$tracks = $mysqli->query("select track_title, track_id from tracks;");
				foreach ($tracks as $id => $track)
				{
					echo '<tr>';
					echo '<td>' . $track['track_title'] . '</td>';
					echo '<td>';
					echo '  <a href="vote.php?from=all_songs&track_id=' . $track['track_id'] . '">';
					echo '    <button type="button" class="btn btn-default">';
					echo '      <span class="glyphicon glyphicon-headphones"></span> Vote';
					echo '    </button>';
					echo '  </a>';
					echo '</td>';
					echo '</tr>';
				}
			?>
		</table>
	</div>
</div>
<?php
  include "footer.php";
?>

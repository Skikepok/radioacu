drop database music;
create database music CHARACTER SET utf8;

use music;

create table if not exists tracks (
track_id int(11) auto_increment,
track_title char(255) not null,
track_authors char(255) default 'unknown',
track_path text not null,
primary key (track_id)
);

create table if not exists votes (
votes_id int(11) auto_increment,
user_login char(255) not null,
track_id int(11) not null,
primary key (votes_id)
);

alter table votes add constraint SongUser unique (track_id, user_login);

create table if not exists history (
history_id int(11) auto_increment,
track_id int(11) not null,
play_date timestamp,
primary key (history_id)
);

create table if not exists proposals (
proposal_id int(11) auto_increment,
user_login char(255) not null,
youtube_link char(255) not null,
status char(255) default "Waiting approval...",
tries int(11) default 0,
primary key (proposal_id)
);

-- insert into tracks (track_title, track_path) values
-- ('Name 1', 'path1'),
-- ('Name 5', 'path2'),
-- ('Name 3', 'path3'),
-- ('Name 4', 'path4');

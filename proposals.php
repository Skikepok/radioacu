<?php
  $mysqli = new mysqli('localhost', 'root', 'jesuisuneradio', 'music');
  $mysqli->set_charset("utf8");
  $tracks = $mysqli->query("select track_title, play_date from history h left join tracks t on t.track_id = h.track_id order by play_date DESC limit 1;");
  $current_song = $tracks->fetch_row()[0];
  include "login.php";
  include "header.php";
?>

<script type="text/javascript">
	var own = document.getElementById('proposals');
	own.className = "active";
</script>

<div class="row">
	<div id="tracks" class="col-md-8">
		<h3>My Proposals</h3>

		<table class="table table-striped">
			<tr>
				<th>Youtube link</th>
				<th>Status</th>
			</tr>
			<?php
				if (!$props = $mysqli->query("select * from proposals where user_login = '$login';"))
					die($mysqli->error);

				foreach ($props as $id => $prop)
				{
					$link = htmlentities(base64_decode($prop['youtube_link']));
					echo '<tr>';
					echo '<td>';
					echo "<a href=\"$link\">$link</a>";
					echo '  <a class="right_float" href="unprop.php?prop_id=' . $prop['proposal_id'] . '">';
					echo '    <button type="button" class="btn btn-default">';
					echo '        <span class="glyphicon glyphicon-remove-circle"></span>';
					echo '    </button>';
					echo '  </a>';
					echo '</td>';
					if ($prop['status'] == "Waiting Download")
						echo '<td>' . $prop['status'] . ': Try number '. $prop['tries'] . '</td>';
					else
						echo '<td>' . $prop['status'] . '</td>';

					echo '</tr>';
				}
			?>
		</table>
	</div>
	<div class="col-md-4 right_float pad">
		<form role="form" method="post" action="propose_yt.php">
			<div class="form-group">
				<label for="link">Youtube Link</label>
				<input type="text" class="form-control" id="link" name="link" placeholder="http://www.youtube.com/watch?v=87p53rAD7Sk">
			</div>
			<button type="submit" class="btn btn-default">Propose</button>
		</form>
	</div>

<?php
  include "footer.php";
?>